FROM docker:20.10.12-dind-alpine3.15
RUN apk add -U docker-compose supervisor
COPY supervisord.conf /etc/supervisord.conf
ENTRYPOINT ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]
WORKDIR /app